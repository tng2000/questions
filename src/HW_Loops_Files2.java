import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class HW_Loops_Files2 {
    public static void main(String[] args) {
        Scanner input = null;
        try {
            input = new Scanner(new FileInputStream("courseData.txt"));
            // read the weights
            double programWeight = input.nextDouble();
            double midtermWeight = input.nextDouble();
            double finalWeight = input.nextDouble();
            // read until the end of the line
            input.nextLine();
            String line = "";
            do {
                line = input.nextLine();
                if (!line.isEmpty()) {
                    int classId = Integer.parseInt(line);
                    if (classId != 0) {
                        int studentId = 0;
                        printClassHeader(classId);
                        do {
                            studentId = input.nextInt();
                            int studentProgram = 0, studentMid = 0, studentFin = 0;
                            if (studentId != 0) {
                                studentProgram = input.nextInt();
                                studentMid = input.nextInt();
                                studentFin = input.nextInt();
                                input.nextLine();
                                double avg = studentProgram * programWeight + studentMid * midtermWeight + studentFin * finalWeight;
                                String grade = "Pass";
                                if (avg <= 50.0) {
                                    grade = "Fail";
                                }
                                System.out.println(String.format("%d       %d       %d      %d           %.2f        %s",
                                        studentId, studentProgram, studentMid, studentFin, avg, grade));
                            }
                        } while (studentId != 0);
                        input.nextLine();
                    }
                }
            } while (input.hasNext() && !line.isEmpty());
        } catch (IOException e) {
            System.out.println("Can't read file " + e.getMessage());
        }

    }

    public static void printClassHeader(int classId) {
        System.out.println();
        System.out.println("Grade Data For Class " + classId);
        System.out.println();
        System.out.println(" ID   Programs  Midterm  Final  Weighted Average  Programs grade");
        System.out.println(" --   --------  -------  -----  ----------------  --------------");
        System.out.println();
    }
}

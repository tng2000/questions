import java.io.*;
import java.util.Random;
import java.util.Scanner;

public class Lab6 {
    public static void randomFile(int count) {
        PrintWriter pw = null;
        Random rnd = new Random();
        try {
            pw = new PrintWriter(new FileOutputStream("fileIn.txt"));
            for (int i = 0; i < count; i++) {
                double num = rnd.nextDouble() * 200 - 50;
                pw.println(num);
            }
        } catch (IOException e) {
            System.out.println("Couldn't open the fileIn.txt file");
        } finally {
            if (pw != null) {
                pw.close();
            }
        }
    }

    public static void stats() {
        Scanner input = null;
        PrintWriter output = null;
        double num;
        double min = 0.0;
        double max = 0.0;
        double avg = 0.0;
        double total = 0.0;
        int below0 = 0;
        int above100 = 0;
        int btwn0and100 = 0;
        int count = 0;
        try {
            input = new Scanner(new FileInputStream("fileIn.txt"));
            while(input.hasNext()) {
                num = input.nextDouble();
                count++; 
                total += num;
                if (num < min) { min = num; }
                if (num > max) { max = num; }
                if (num < 0.0) {
                    below0++;
                } else {
                    if (num < 100) {
                        btwn0and100++;
                    }
                    else {
                        above100++;
                    }
                }
            }
            if (count > 0) {
                avg = total / count;
            }
            System.out.println("Min: " + min );
            System.out.println("Max: " + max );
            System.out.println("Count: " + count);
            System.out.println("Below 0: " + below0 + ", between 0 and 100 : " + btwn0and100 + ", above 100: " + above100);
            System.out.println("Average: " + avg);
        } catch (IOException e) {
            System.out.println("Can't open file");
        } finally {
            if (null != input) {
                input.close();
            }
        }
    }
}

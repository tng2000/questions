package genome;

public class Stats {
	public static int alignments(String dna1, String dna2) {
		if (null == dna1 || null == dna2) {
			return 0;
		}
		
		int max = 0;
		// first we make sure dna1 is always the shortest string
		// if not then we swap dna1 and dna2
		if (dna2.length() < dna1.length()) {
			String t = dna1;
			dna1 = dna2;
			dna2 = t;
		}
		// we will iterate dna2 over dna1 with an space in the beginning, 
		// to cover all the possible comparisons
		// The offset acts like a space ahead of dna2
		int diff = dna2.length() - dna1.length();
		// iterate at least once - in case both strings have the same length
		for (int offset = 0; offset <= diff; offset++) {
			// the score for this iteration is calculated
			int score = 0;
			for (int i = 0; i < dna1.length(); i++) {
				if (dna1.charAt(i) == dna2.charAt(i + offset)) {
					score++;
				}
			}
			// if this iteration's score is greater than the overall, the we put this as the overall
			if (score > max) {
				max = score;
			}
		}
		// we may return 0 if there wasn't any alignment found
		return max;
	}
}

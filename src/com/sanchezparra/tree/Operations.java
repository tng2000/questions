package com.sanchezparra.tree;

import java.util.Scanner;

public class Operations {

    public String serialize(Node node) {
        String answer = "";
        if (node == null) {
            return "";
        }

        answer += Integer.toString(node.getValue());
        if (node.getLeft() != null) {
            answer += ":" + serialize(node.getLeft());
        } else {
            answer += ":" + "-";
        }
        if (node.getRight() != null) {
            answer += ":" + serialize(node.getRight());
        } else {
            answer += ":" + "-";
        }
        return answer;
    }

    private Scanner tree = null;

    public Node deserialize(String input) {
        if (null == tree) {
            tree = new Scanner(input);
            tree.useDelimiter(":");
        }
        return deserialize();
    }
    
    public Node deserialize() {
        Node answer = null;
        if (tree.hasNext()) {
            String next = tree.next();
            if (next.equals("-")) {
                return null;
            }
            answer = new Node(Integer.parseInt(next));
        }
        if (tree.hasNext()) {
            answer.setLeft(deserialize() );
        }
        if (tree.hasNext()) {
            answer.setRight(deserialize());
        }
        return answer;
    }
}

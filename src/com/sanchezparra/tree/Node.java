package com.sanchezparra.tree;

public class Node {
    private int value;
    private Node left;
    private Node right;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public Node(int data) {
        this();
        value = data;
    }

    public Node() {
        left = null;
        right = null;
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }

    @Override
    public boolean equals(Object obj) {
        if (null != obj && obj instanceof Node) {
            Node o = (Node) obj;
            return value == ((Node)obj).getValue() 
                    && ((left != null && left.equals(o.left) || (left == null && o.left == null)))
                    && ((right != null && right.equals(o.right)) || (right == null && o.right == null));
           
        }
        return false;
    }
}

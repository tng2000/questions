package com.sanchezparra.data;

public class TreeNode {
    int val;
    int leftWeight; // number of nodes on the left side 0 means no nodes
    int rightWeight; // number of nodes on the right hand side. Negative is not valid
    TreeNode left;
    TreeNode right;
}

package com.sanchezparra.data;

public class TreeOperations {

    //
    // 10
    // 5 13
    // 2 6 11 12
    //
    //
    // 2 5 6 10 11 13 12
    // k = 3 (ans = 6) // works
    // k = 4 (ans = 10)
    // k = 7 (ans = 12)
    //
    //
    // root
    // v = 10
    // lW = 3
    // rW = 3

    /**
     * Question from FB. First without weight on each side. Then with weight on each side
     * @param root The root of the tree, can't be null
     * @param k a number equals or greater than 1, indicating the order for the number to return
     * @return The node in order as requested, or null if k is larger than the number of nodes in the tree
     * @throws InvalidKException
     */
    public TreeNode getkthsmallest(TreeNode root, int k) throws InvalidKException {
        if (k < 1) {
            throw new InvalidKException(k + " is not a valid value");
        }
        if (root == null) {
            return null;
        }

        // 3 exclusive options:
        // k is on the left
        // k is the root
        // k is no the right

        // k < leftW -> it is on the left
        // k = (left + 1) it is root!!! and return it
        // what happens on the right?

        if (k == root.leftWeight + 1) {
            return root;
        }
        if (k <= root.leftWeight) {
            return getkthsmallest(root.left, k);
        }
        k -= (root.leftWeight + 1); // k = 7, lW = 3 k= 7 - (3+1) => 3// k= 3 - (1 +1 ) => 1
        if (k > 0) {
            return getkthsmallest(root.right, k);
        }
        return null;

        // if (root.l != null) {
        // answer = getkthsmallest(root.l, k);
        // if (null != answer) {
        // return answer;
        // }
        // }
        // position++;
        // if (k == position) {
        // position = -1;
        // return root;
        // }
        // if (root.r != null) {
        // answer = getkthsmallest(root.r, k);
        // }
        // return answer;
    }
}

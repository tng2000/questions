package com.sanchezparra.data;

public class InvalidKException extends Exception {

    public InvalidKException(String message) {
        super(message);
    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}

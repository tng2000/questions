package trees;

import java.util.*;

// Doesn't work
public class Operations {
    private Deque<TreeNode> stack = new ArrayDeque<>();
    private List<TreeNode> forest = new ArrayList<>();
    
    public List<TreeNode> eraseNodes(TreeNode root, List<Integer> TBD) {
        if (root == null || null == TBD || TBD.isEmpty()) {
            return forest;
        }
        eraseNodes(null, root, TBD);
        return forest;
    }

    public void eraseNodes(TreeNode parent, TreeNode root, List<Integer> TBD) {
        if (root == null || TBD.isEmpty()) {
            return;
        }

        System.out.println(root);
        if (root.left != null) {
            eraseNodes(root, root.left, TBD);
            if (root.left.val == null) {
                root.left = null;
            }
        }
        if (root.right != null) {
            eraseNodes(root, root.right, TBD);
            if (root.right.val == null) {
                root.right = null;
            }
        }
        if (TBD.remove(root.val)) {
            root.val = null;
            if (root.left != null) {
                forest.add(root.left);
            }
            if (root.right != null) {
                forest.add(root.right);
            }
        } else {
            if(parent == null) {
                forest.add(root);
            }
        }
        System.out.println("TBD: " + Arrays.toString(TBD.toArray()) + " forest: " + Arrays.toString(forest.toArray()));
        return;
    }

}

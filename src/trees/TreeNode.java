package trees;

public class TreeNode {
    public Integer val;
    public TreeNode left = null;
    public TreeNode right = null;

    TreeNode(int x) {
        val = x;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(val);
        if (left != null) {
            sb.append(' ');
            sb.append(left.val);
            sb.append(' ');
        } else {
            sb.append(" N");
        }

        if (right != null) {
            sb.append(' ');
            sb.append(right.val);
            sb.append(' ');
        } else {
            sb.append(" N");
        }
        return sb.toString();
    }
}

package trees;

import java.util.*;

public class Symmetric {

    Map<Integer, List<Integer>> map = new HashMap<>();

    public boolean isSymmetric(TreeNode root) {
        if (root == null)
            return true;
        if (root.left == null && root.right == null)
            return true;
        if (root.left == null || root.right == null)
            return false;
        stringPerLevel(root, 0);
        for (int key : map.keySet()) {
            Integer[] l1 = new Integer[map.get(key).size()];
            l1  = map.get(key).toArray(l1);
            int tot = l1.length / 2 + 1;
            for(int i = 0; i < tot; i++ ) {
                if (l1[i] != l1[l1.length -i - 1]) {
                    return false;
                }
            }
        }
        return true;
    }
    
    private void addString(int level, Integer val) {
        if (!map.containsKey(level)) {
            map.put(level, new ArrayList<>());
        }
        List<Integer> sb = map.get(level);
        sb.add(val);
        map.put(level, sb);
    }

    private void stringPerLevel(TreeNode root, int level) {
        if (root == null) {
            return;
        }
        if (root.left == null) {
            addString(level, null);
        } else {
            addString(level, root.left.val);
            stringPerLevel(root.left, level + 1);
        }
        if (root.right == null) {
            addString(level, null);
        } else {
            addString(level, root.right.val);
            stringPerLevel(root.right, level + 1);
        }
    }
}

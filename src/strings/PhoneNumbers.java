package strings;

import java.util.HashMap;
import java.util.Map;

public class PhoneNumbers {
    private Map<Integer, String> m1 = new HashMap<>();

    public PhoneNumbers() {
        m1.put(1, "");
        m1.put(2, "ABC");
        m1.put(3, "DEF");
        m1.put(4, "GHI");
        m1.put(5, "JKL");
        m1.put(6, "MNO");
        m1.put(7, "PQRS");
        m1.put(8, "TUV");
        m1.put(9, "WXYZ");
        m1.put(0, "");
    }

    public void strings(long number) {
        System.out.println("----- " + number);
        strings(Long.toString(number), 0, "");
    }

    public void strings(String number, int idx, String buf) {
        if (idx >= number.length() || idx < 0) {
            System.out.println(buf);
            return;
        }
        int d = (int)number.charAt(idx) - '0';
        String ns = m1.get(d);
        if (ns.length() == 0) {
            strings(number, idx + 1, buf);
        }
        for (int i = 0; i < ns.length(); i++) {
            if (idx < number.length()) {
                strings(number, idx + 1, buf + ns.charAt(i));
            }
        }
    }
}

package arrays;

import java.util.Arrays;

public class Rotate {
    public void rotate(int[] nums, int k) {
        k = k % nums.length;
       if (k == nums.length || k == 0) 
           return;
        int move = nums.length - k;
        int[] temp = new int[k];
        int kt = k;
        for(int i = nums.length - 1; kt > 0; i--) {
            kt--;
            temp[kt] = nums[i];
        }
        for(int i = move - 1; i >= 0; i--) {
            nums[i+k] = nums[i];
        }
        for(int i = 0; i < k; i++) {
            nums[i] = temp[i];
        }
        System.out.println("n: " + nums.length + ", k = " + k);
        System.out.println(Arrays.toString(temp));
        System.out.println(Arrays.toString(nums));
    }
}

package arrays;

import java.util.ArrayList;
import java.util.List;

public class PlusOne {
    public int[] plusOne(int[] digits) {
        boolean carry = true;
        List<Integer> ans = new ArrayList<Integer>();
        int i = 0;
        for (i = digits.length - 1; i >= 0; i--) {
            if (carry) {
                digits[i]++;
                carry = false;
                if (digits[i] > 9) {
                    carry = (digits[i] / 10) > 0;
                    digits[i] %= 10;
                }
            }
            ans.add(0, digits[i]);
        }
        if (carry) {
            ans.add(0, 1);
        }
        int[] t = new int[ans.size()];
        int idx = 0;
        for (int d : ans) {
            t[idx] = d;
            idx++;
        }
        return t;
    }
}

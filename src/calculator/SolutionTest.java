package calculator;

import static org.junit.Assert.*;

import org.junit.Test;

public class SolutionTest {

    @Test
    public void testPlus() {
        int ret = new Solution().calculate( "1+1");
        assertEquals(2,ret);
    }
    
    @Test
    public void testPa1() {
        int ret = new Solution().calculate("123  +   (456 -  789) ");
        assertEquals(-210, ret);
    }
    
    @Test
    public void test4() {
        int ret = new Solution().calculate("    (   1   +   1) +  2");
        assertEquals(4,ret);
    }
    
    @Test 
    public void test12() {
        int ret = new Solution().calculate("(2+6* 3+5- (3*14/7+2)*5)+3");
        assertEquals(12, ret);
    }
    
    @Test
    public void testPa2() {
        assertEquals(3, new Solution().calculate("1+(2)"));
    }

}

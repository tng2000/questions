package calculator;


class Solution {
    public int calculate(String s) {
        String exp = s.replaceAll("\\s","");
        System.out.println(exp);
        StringBuilder left = new StringBuilder();
        int value = 0;
        int lvalue = 0;
        char op = '\0';
        for(int i = 0; i < exp.length(); i++) {
            char c = exp.charAt(i);
            if (c >= '0' && c <= '9') {
                value *= 10;
                value += Character.getNumericValue(c);
            } else {
                switch (exp.charAt(i)){ 
                    case '(':
                        int r = findClosingP(exp, i);
                        if (r < 0) {
                            System.out.println("ERROR");
                            return 0;
                        }
                        value = calculate(exp.substring(i+1, r));
                        i = r ;
                        break;
                    case '-':
                    case '+':
                    case '*':
                    case '/':
                        if (op == '\0') {
                            lvalue = value;
                        } else {
                            lvalue = operate(op, lvalue, value);
                        }
                        value = 0;
                        op = exp.charAt(i);
                        break;
                    default:
                        break;
                }
            }
        }
        return operate(op, lvalue, value);
    }
    
    private int operate(char op, int lvalue, int value) {
        switch (op) {
            case '-':
                value = lvalue - value;
                break;
            case '+':
                value = lvalue + value;
                break;
            case '*':
                value= lvalue * value;
                break;
            case '/':
                value = lvalue / value;
                break;
        }
        return value;
    }
    
    private int findClosingP(String exp, int s) {
        int p = 0;
        for(int i = s; i < exp.length(); i++ ){
            if (exp.charAt(i) == '(') {
                p++;
            }
            if (exp.charAt(i) == ')') {
                p--;
            }
            if (p==0) {
                return i;
            }
        }
        return -1;
    }
    
}

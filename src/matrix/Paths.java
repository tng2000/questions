package matrix;

import java.util.*;

public class Paths {

    private Map<Integer, List<Integer>> visited = new HashMap<>();

    public int longPath(int[][] matrix) {
        int ans = 0;
        List<Integer> path = null;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                List<Integer> pt = new ArrayList<>();
                int s = findPath(matrix, i, j, 0, pt);
                if (s > ans) {
                    ans = s;
                    path = pt;
                }
            }
        }
        System.out.println("Best path (" + ans + ") :" + Arrays.toString(path.toArray()));
        return ans;
    }

    private int findPath(int[][] matrix, int i, int j, int steps, List<Integer> pathSoFar) {
        int point = i * matrix.length + j;
        int val = matrix[i][j];
        int l = 0, r = 0, u = 0, d = 0;
        pathSoFar.add(val);

        int ans = steps;
        List<Integer> ansP = null;

        List<Integer> pl = new ArrayList<>();
        List<Integer> pr = new ArrayList<>();
        List<Integer> pu = new ArrayList<>();
        List<Integer> pd = new ArrayList<>();
        // left
        if (j > 0) {
            if (val < matrix[i][j - 1]) {
                l = findPath(matrix, i, j - 1, steps + 1, pl);
            }
        }
        // right
        if (j < matrix.length - 1) {
            if (val < matrix[i][j + 1]) {
                r = findPath(matrix, i, j + 1, steps + 1, pr);
            }
        }
        // up
        if (i > 0) {
            if (val < matrix[i - 1][j]) {
                u = findPath(matrix, i - 1, j, steps + 1, pu);
            }
        }
        // down
        if (i < matrix[i].length - 1) {
            if (val < matrix[i + 1][j]) {
                d = findPath(matrix, i + 1, j, steps + 1, pd);
            }
        }
        if (ans < r) {
            ans = r;
            ansP = pr;
        }
        if (ans < l) {
            ans = l;
            ansP = pl;
        }
        if (ans < u) {
            ans = u;
            ansP = pu;
        }
        if (ans < d) {
            ans = d;
            ansP = pd;
        }

        visited.put(point, ansP);
        if (null != ansP) {
            pathSoFar.addAll(ansP);
            System.out.println(Arrays.toString(pathSoFar.toArray()));
        }
        return ans;
    }

}

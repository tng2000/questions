package trees;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import org.junit.Test;

public class RemoveNodeTest {
    
    private Operations op = new Operations();
    
    private List<Integer> toList(int... values) {
        List<Integer> list = new ArrayList<>(values.length);
        for(int i : values) {
            list.add(new Integer(i));
        }
        return list;
    }
    
    private List<Integer> toList(List<TreeNode> BT) {
        List<Integer> list = new ArrayList<>();
        BT.forEach(n->list.add(n.val));
        return list;
    }
    
    @Test
    public void testBase() {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        
        List<TreeNode> ans = op.eraseNodes(root, toList(4,3));
        
        assertArrayEquals(toList(6,7,1).toArray(), toList(ans).toArray());
    }

    @Test
    public void testNullTBD() {
        TreeNode root = new TreeNode(1);
        op.eraseNodes(root, null);
    }
    
    @Test
    public void testAllNull() {
        op.eraseNodes(null, toList(1));
    }
    
    @Test
    public void testOneNode() {
        TreeNode root = new TreeNode(1);
        List<TreeNode> ans = op.eraseNodes(root,  toList(1));
        assertEquals(0, ans.size());
    }
}

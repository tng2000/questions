package trees;

import java.util.*;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Traversal {
    
    Map<Integer, List<Integer>> trav = new HashMap<>();
    public List<List<Integer>> levelOrder(TreeNode root) {
        trav = new HashMap<>();
        if (root != null) {
            trav(root, 0);
        }
        List<List<Integer>> answer = new ArrayList<>();
        for(Integer key : trav.keySet()) {
            answer.add(trav.get(key));
        }
        return answer;
    }
    
    public void trav(TreeNode root, int level) {
        if (!trav.containsKey(level)) {
            trav.put(level, new ArrayList<>());
        }
        List<Integer> list = trav.get(level);
        list.add(root.val);
        if (null != root.left) {
            trav(root.left, level + 1);
        }
        if (null != root.right) {
            trav(root.right, level + 1);
        }
    }
}
package trees;


import static org.junit.Assert.*;

import org.junit.Test;


public class SymmetricTest {

    private Symmetric s = new Symmetric();

    @Test
    public void test() {
        TreeNode root = new TreeNode(2);
        root.left = new TreeNode(3);
        root.right = new TreeNode(3);
        assertTrue(s.isSymmetric(root));
    }
    
    @Test
    public void notest() {
        TreeNode root = new TreeNode(2);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        assertFalse(s.isSymmetric(root));
    }
    
    @Test
    public void nulls() {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(2);
        root.left.right = new TreeNode(3);
        root.right.right = new TreeNode(3);
        assertFalse(s.isSymmetric(root));
    }

}

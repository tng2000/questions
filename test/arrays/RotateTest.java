package arrays;
import static org.junit.Assert.*;

import org.junit.Test;

public class RotateTest {

    @Test
    public void rotate1() {
        Rotate r = new Rotate();
        r.rotate(new int[] { 1,  2, 3, 4, 5 }, 1);
    }
    
    @Test
    public void rotate2() {
        Rotate r = new Rotate();
        r.rotate(new int[] { 1,  2, 3, 4, 5 }, 2);
    }
    
    @Test
    public void rotate4() {
        Rotate r = new Rotate();
        r.rotate(new int[] { 1,  2, 3, 4, 5 }, 4);
    }
    
    @Test
    public void rotate7() {
        Rotate r = new Rotate();
        r.rotate(new int[] { 1,  2, 3, 4, 5 }, 7);
    }

}

package arrays;

import java.util.Arrays;

import org.junit.*;

public class PlusOneTest {

    private PlusOne p;

    @Before
    public void setup() {
        p = new PlusOne();
    }
    
    @Test
    public void test() {
        int [] ans = p.plusOne(new int[] { 0 });
        System.out.println(Arrays.toString(ans));
        Assert.assertArrayEquals(new int[] { 1 }, ans);
    }
    
    @Test
    public void nine() {
        int [] ans = p.plusOne(new int[] { 9 });
        System.out.println(Arrays.toString(ans));
        Assert.assertArrayEquals(new int[] { 1, 0 }, ans);
    }
    
    @Test
    public void eleven() {
        int [] ans = p.plusOne(new int[] { 1, 0 });
        System.out.println(Arrays.toString(ans));
        Assert.assertArrayEquals(new int[] { 1, 1 }, ans);
    }
    
    public boolean equal(int[] a1, int [] a2) {
        if(a1.length != a2.length)
            return false;
        for(int i = 0; i < a1.length ; i++) {
            if ( a1[i] != a2[i]) {
                return false;
            }
        }
        return true;
    }

}

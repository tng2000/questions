package matrix;

import static org.junit.Assert.*;

import org.junit.Test;

public class PathsTest {

    private Paths p = new Paths();

    @Test
    public void test() {
        int matrix[][] = new int[][] { { 9, 9, 4 }, { 6, 6, 8 }, { 2, 1, 1 } };
        int s = p.longPath(matrix);
        assertEquals(3, s);
    }
    
    @Test
    public void test2() {
        int matrix[][] = new int[][] { { 3, 4, 5 }, { 3, 2, 6 }, { 2, 2, 1 } };
        int s = p.longPath(matrix);
        assertEquals(3, s);
    }

    @Test
    public void test3() {
        int matrix[][] = new int[][] { { -5, 3, 6, 9}, { -6, -9, 10, 20}, {-20, -15, 10, 3}, {13, 2, 1, 0} };
        int s =p.longPath(matrix);
    }
}

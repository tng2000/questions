package genome;

import org.junit.Test;

public class StatsTest {
	@Test
	public void test1() throws Exception {
		int score = Stats.alignments("AATCTATA", "AAGATA");
		System.out.println(score);
	}

	@Test
	public void test2() throws Exception {
		int score = Stats.alignments("AAGATA", "AATCTATA");
		System.out.println(score);
	}
	
	@Test
	public void test3() throws Exception {
		Stats.alignments("", "");
	}
}

package strings;

import org.junit.Before;
import org.junit.Test;

public class PhoneNumbersTest {
    PhoneNumbers pn = new PhoneNumbers();
    @Before
    public void setup() {
        
    }
    @Test
    public void simpleTest() throws Exception {
        pn.strings(1023456789L);
    }
    
    @Test
    public void empty() throws Exception {
        pn.strings(10101010);
    }
    
    @Test
    public void single() throws Exception {
        pn.strings(9);
    }
    
    @Test
    public void dups() throws Exception {
        pn.strings(989898);
    }
}
